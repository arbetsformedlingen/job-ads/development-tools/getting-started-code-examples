import requests
import json

"""
Install python packages:
pip install -r requirements.txt

Add your api-key!
"""

api_key = ''

url = 'https://jobsearch.api.jobtechdev.se'
url_for_search = f"{url}/search"


def _get_ads(params):
    headers = {'api-key': api_key, 'accept': 'application/json'}
    response = requests.get(url_for_search, headers=headers, params=params)
    response.raise_for_status()  # check for http errors
    return json.loads(response.content.decode('utf8'))


def example_search_return_number_of_hits():
    query = 'lärare'
    limit = 0  # limit: 0 means no ads, just a value of how many ads were found
    search_params = {'q': query, 'limit': limit}
    json_response = _get_ads(search_params)
    number_of_hits = json_response['total']['value']
    print(number_of_hits)


def example_search_loop_through_hits():
    query = 'lärare'
    limit = 100  # 100 is max number of hits that can be returned.
    #  If there are more (which you find with ['total']['value'] in the json response)
    #  you have to use offset and multiple requests to get all ads
    search_params = {'q': query, 'limit': limit}
    json_response = _get_ads(search_params)
    hits = json_response['hits']
    for hit in hits:
        print(f"{hit['headline']}, {hit['employer']['name']}")


if __name__ == '__main__':
    example_search_return_number_of_hits()
    example_search_loop_through_hits()

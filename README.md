# Getting started - code examples 

Code examples for using Jobtech APIs.

### JobSearch
A search API for all currently published ads at Arbetsförmedlingen/Platsbanken, 
available here https://jobsearch.api.jobtechdev.se

Example file: `jobsearch_examples.py` 

In order to run this you need to retrieve an API key and enter at the top of the file. 
This key can be retrieved from https://apirequest.jobtechdev.se.

### JobStream
If you want to download all ads, rather than searching for specific ads, it's JobStream you should use.
It's exactly the same data set as JobSearch, and it has an endpoint for getting real-time updates of changes.

A getting started code example for JobStream is found in a different repository: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobstream-example

### JobTech Links
This is a larger data set including also ads from external job ad providers. 
However, there is less information per ad. E.g. the text body of the ad is lacking, instead there is a brief description.
Available here:  https://links.api.jobtechdev.se

Example file: `jobtechlinks_examples.py` 

## Getting started with Python

Prerequisites: Python 3.7+<br />
Install python packages with: `pip install -r requirements.txt`

